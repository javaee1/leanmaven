# LeanMaven

## Maven Commands
**The most commonly used build phases in the default build life cycle are:**

| Build Phase        | Description           | 
| ------------- |-------------|
| validate     |alidates that the project is correct and all necessary information is available. This also makes sure the dependencies are downloaded. |
| clean      | delete all previously compiled code other words delete the target folder.      | 
| compile | Compiles the source code of the project.     |
| package | Packs the compiled code in its distributable format, such as a JAR.     |
| test | Install the package into the local repository, for use as a dependency in other projects locally.     |
| deploy | Copies the final package to the remote repository for sharing with other developers and projects.      |

## Maven Lifecycle
 **`validate`** - validate the project is correct and all necessary information is available.

**`compile`** - compile the source code of the project.

**`test`** - test the compiled source code using a suitable unit testing framework. These tests should not require the code be packaged or deployed

**`package`** - take the compiled code and package it in its distributable format, such as a JAR.

**`verify`** - run any checks on results of integration tests to ensure quality criteria are met.

**`install`** - install the package into the local repository, for use as a dependency in other projects locally.

**`deploy`** - done in the build environment, copies the final package to the remote repository for sharing with other developers and projects.

`Note:- ` For more reference [click](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html#Lifecycle_Reference) here.

## Cheat Sheet

![maven cheat sheet](.doc/Maven-Commands-Cheat-Sheet.png)

## Reference Commands
```bash
 $ lsof -i -P -n | grep LISTEN
 $ docker container run --rm -it -p 8085:8080 --name learnMaven -v `pwd`:/code maven bash
 $ mvn package -Dmaven.test.skip=true
```